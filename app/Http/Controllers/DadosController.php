<?php

namespace App\Http\Controllers;

use App\Models\Dados;
use App\Models\File;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DadosController extends Controller
{
    public function index()
    {
        $dadosFile = File::with('dados')->get();
        return view('dados.index', compact('dadosFile'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        if ($request->file == null) {
            return redirect()->back()->with('message', "Você deve escolher um arquivo válido");
        }
        try {
            $dado = new Dados;
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();

            $createFile = File::create([
                'nome_arquivo' => $fileName,
            ]);

            $myfile = fopen($file, "r") or die("Unable to open file!");
            $dados = fread($myfile, filesize($file));

            $objDados = explode("\n", $dados);
            for ($i = 1; $i < count($objDados); $i++) {
                $result = explode("\t", $objDados[$i]);
                $somaValorTotal[] = $result[2] * $result[3];
                $dado->create([
                    'comprador' => $result[0],
                    'descricao' => $result[1],
                    'preco_unitario' => $result[2],
                    'quantidade' => $result[3],
                    'endereco' => $result[4],
                    'fornecedor' => $result[5],
                    'file_id'  => $createFile->id
                ]);
            }
            fclose($myfile);
            DB::commit();
            return redirect()->route('dados.index');
        } catch (Exception $e) {

            return redirect()->back()->with('error', $e->getMessage());
            DB::rollBack();
        }
    }

    public function receita()
    {
        return view('dados.receita');
    }

    public function show(Request $request, $id = null)
    {
        $idFile = $request->file;
        $somaValorTotal = array();

        $dados = Dados::with('file')->where('file_id', $idFile)->get();
        for ($i = 0; $i < count($dados); $i++) {
            $somaValorTotal[] = ($dados[$i]['preco_unitario'] * $dados[$i]['quantidade']);
        }
        $resultSoma = array_sum($somaValorTotal);
        return response()->json([$dados, $resultSoma]);
    }
}
