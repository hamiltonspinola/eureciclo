<form name="fomReceita">
    <div class="row">
        <select name="file" id="file" class="form-control col-md-8" style="width: 80%">
            <option value=""></option>
            @foreach ($dadosFile as $f)
                <option value="{{ $f->id }}">{{ $f->nome_arquivo }}</option>
            @endforeach
            <input type="submit" value="Consultar" class="btn btn-info col-md-2">
        </select>
    </div>
</form>