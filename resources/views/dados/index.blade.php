<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="card">
                    <div class="card-header">

                    </div>

                    <div class="card-body">
                        <form action="{{ route('dados.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group col-md-8">
                                <label for="file">Upload de arquivos</label>
                                <input type="file" class="form-control-file" id="file" name="file">
                            </div>
                            <div class="form-group col-md-3">
                                <button type="submit" class="btn btn-info">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="container">

            <div class="row">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            @include('dados.receita')
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr class="lReceita">
                                    <label id="receita">
                                    </label>
                                </tr>
                                <tr>
                                    <th>
                                        Comprador
                                    </th>
                                    <th>
                                        Descrição
                                    </th>
                                    <th>
                                        Valor
                                    </th>
                                    <th>
                                        Quantidade
                                    </th>
                                    <th>
                                        Endereço
                                    </th>
                                    <th>
                                        Fornecedor
                                    </th>
                                </tr>
                            </thead>

                            <tbody class="tbody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    
    <script>
        $(function(){
            $('form[name="fomReceita"]').submit(function(event) {
                event.preventDefault();

                $.ajax({
                    url: "{{ route('dados.show') }}",
                    type: "GET",
                    data: $(this).serialize(),
                    dataType: 'json',
                    success:function(response){
                        var tbody = $('table').find('.tbody');
                        var trReceita = $('#receita').html("<h4 class='card-title'> Receita Total: "+response[1]+"</h4>");
                        console.log(trReceita);
                        for(var i=0; i<response[0].length; i++){
                            tbody.append("<tr><td>"+response[0][i]['comprador']+"</td><td>"+response[0][i]['descricao']+"</td><td>"+response[0][i]['preco_unitario']+"</td><td>"+response[0][i]['quantidade']+"</td><td>"+response[0][i]['endereco']+"</td><td>"+response[0][i]['fornecedor']+"</td></tr>");
                        }
                    }
                });
            });
        });
    </script>
</body>

</html>
