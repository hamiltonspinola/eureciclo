<?php

namespace Tests\Unit;

use Tests\TestCase;

class ControllerTest extends TestCase
{
    public function test_method_index()
    {
        $this->call('GET', '/');
        $this->assertTrue(true);
    }
    public function test_method_store()
    {
        $dados = $this->call('POST',route('dados.store'), [
            'comprador' => "Hamilton",
            'descricao' => "Teste",
            'preco_unitario' => 10,
            'quantidade' => 3,
            'endereco' => "Rua tal",
            'fornecedor' => "Bob's",
            'file_id'  => 1
        ]);

        $dados->assertStatus(302);
    }
}
