<?php

namespace Tests\Unit;

use App\Models\Dados;
use App\Models\File;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class ModelsTest extends TestCase
{
    use DatabaseTransactions;
  

    public function test_insert_file_in_database()
    {
        File::create([
            'nome_arquivo' => 'file.txt',
        ]);

        $this->assertDatabaseHas('files', ['nome_arquivo'  => 'file.txt']);
    }

    public function test_insert_dados_in_database()
    {
        $file = File::create([
            'nome_arquivo' => 'file.txt',
        ]);
        
        $dados = Dados::create([
            'comprador' => 'Hamilton',
            'descricao' => 'Descrição',
            'preco_unitario'    => 10,
            'quantidade'    => 2,
            'endereco'      => 'Rua do bobo',
            'fornecedor'    => 'Bobs',
            'file_id'       => $file->id,
        ]);

        $this->assertDatabaseHas('files', ['nome_arquivo'  => 'file.txt']);
        $this->assertDatabaseHas('dados', [
            'file_id'  => $file->id,
            'comprador' => 'Hamilton',
            'descricao' => 'Descrição',
            'endereco'  => 'Rua do bobo'
        ]);
    }

    public function test_get_dados_by_file(){
        $file = File::create([
            'nome_arquivo' => 'file.txt',
        ]);
        
        $dados = Dados::create([
            'comprador' => 'Hamilton',
            'descricao' => 'Descrição',
            'preco_unitario'    => 10,
            'quantidade'    => 2,
            'endereco'      => 'Rua do bobo',
            'fornecedor'    => 'Bobs',
            'file_id'       => $file->id,
        ]);

        $this->assertInstanceOf(Collection::class, $file->dados);
        $this->assertInstanceOf(Dados::class, $file->dados->first());
    }

    public function test_get_file_by_dados(){
        $file = File::create([
            'nome_arquivo' => 'file.txt',
        ]);
        
        $dados = Dados::create([
            'comprador' => 'Hamilton',
            'descricao' => 'Descrição',
            'preco_unitario'    => 10,
            'quantidade'    => 2,
            'endereco'      => 'Rua do bobo',
            'fornecedor'    => 'Bobs',
            'file_id'       => $file->id,
        ]);

        $this->assertInstanceOf(File::class, $dados->file);
    }
}
